=======
{{ cookiecutter.project_name }}
=======

This is the documentation of **{{ cookiecutter.project_name }}**.


Contents
========

.. toctree::
   :maxdepth: 2

   License <license>
   Authors <authors>
   Changelog <changelog>
   Module Reference <api/modules>


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

