[![PyPI status](https://img.shields.io/pypi/status/{{ cookiecutter.project_name }}.svg)](https://pypi.python.org/pypi/{{ cookiecutter.project_name }}/)
[![PyPI version](https://img.shields.io/pypi/v/{{ cookiecutter.project_name }}.svg)](https://pypi.python.org/pypi/{{ cookiecutter.project_name }}/)
[![PyPI pyversions](https://img.shields.io/pypi/pyversions/{{ cookiecutter.project_name }}.svg)](https://pypi.python.org/pypi/{{ cookiecutter.project_name }}/)
[![Pipeline status](https://gitlab.com/frkl/{{ cookiecutter.project_name }}/badges/develop/pipeline.svg)](https://gitlab.com/frkl/{{ cookiecutter.project_name }}/pipelines)
[![Code style](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/ambv/black)

# {{ cookiecutter.project_name }}

*{{ cookiecutter.project_short_description }}*


## Description

Documentation still to be done.

# Development

Assuming you use [pyenv](https://github.com/pyenv/pyenv) and [pyenv-virtualenv](https://github.com/pyenv/pyenv-virtualenv) for development, here's how to setup a '{{ cookiecutter.project_name }}' development environment manually:

    pyenv install 3.7.3
    pyenv virtualenv 3.7.3 {{ cookiecutter.project_slug }}
    git clone https://gitlab.com/{{ cookiecutter.gitlab_user }}/{{ cookiecutter.project_slug }}
    cd <{{ cookiecutter.project_slug }}_dir>
    pyenv local {{ cookiecutter.project_slug }}
    pip install -e .[develop,testing,docs]
    pre-commit install


## Copyright & license

Please check the [LICENSE](/LICENSE) file in this repository (it's a short license!), also check out the [*freckles* license page](https://freckles.io/license) for more details.

[Parity Public License 6.0.0](https://licensezero.com/licenses/parity)

[Copyright (c) 2019 frkl OÜ](https://frkl.io)
